package etudiant;

import java.sql.*;

public class menu {
    public static Connection connection;
    public String nom;
    public String date;


    public static void main(String[] args) {
        /*
        System.out.println(" menu pour le choix des etudiant");
        System.out.println(" 1: enregistrement des etudiant");
        System.out.println("2: recherche d'un etudiant selon son nom ou son id");
        System.out.println("3:suppression d'un etudiant");
        System.out.println("4: modification d'un etudiant");
        System.out.println("veillez faire votre choix");
        scanner sc = new scanner(System.in);
        int str = sc.nextInt();
        switch (str) {
            case 1:
                System.out.println(" 1: enregistrement des etudiant");
                break;
            case 1:
                System.out.println(" 1: enregistrement des etudiant");
                break;

        }*/


        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/etudiant", "root", "");
            connection = con;

            con.createStatement().executeUpdate("DELETE FROM renseignement");

//            String requete = "INSERT INTO renseignement(nom,date) values('flora','15-janvier 2022')";
//
//            try {
//                Statement stat = con.createStatement();
//                int nb = stat.executeUpdate(requete);
//                System.out.println("nb mise a jour = " + nb);
//            } catch (SQLException e) {
//                System.out.println("insertion echoue");
//            }
            insererRenseignement("flora", "15-janvier-2020");
            insererRenseignement("herve", "15-janvier-2020");

            //String affiche = "SELECT * from renseignement";

//            ResultSet result = null;
//
//            try {
//                Statement stmt = con.createStatement();
//                result = stmt.executeQuery(affiche);
//                System.out.println("resultat de l affichage ");
//                while (result.next()) {
//                    System.out.println("id: " + result.getInt("id"));
//                    System.out.println("nom: " + result.getString("nom"));
//                    System.out.println("date: " + result.getString("date"));
//
//                }
//            } catch (SQLException e) {
//                System.out.println("affichage echoue");
//            }

        affiche();

//
//            try {
//                PreparedStatement recherche = con.prepareStatement("SELECT*from renseignement where nom=?");
//                recherche.setString(1, "flora");
//                ResultSet resut = recherche.executeQuery();
//                System.out.println("resultat de la recherche");
//                while (resut.next()) {
//
//                    System.out.println("id: " + resut.getInt("id"));
//                    System.out.println("nom: " + resut.getString("nom"));
//                    System.out.println("date: " + resut.getString("date"));
//                }
//            } catch (SQLException e) {
//                System.out.println("recherche echoue");
//            }

            recherche("flora");
            //suppression
//
//            try {
//                PreparedStatement statement = con.prepareStatement("DELETE FROM renseignement WHERE nom=?");
//                statement.setString(1, "flora");
//                statement.executeUpdate();
//            } catch (SQLException e) {
//                System.out.println("suppression echoue");
//            }
            supprimerRenseignement("flora");
            //mise a jours

//            try {
//                PreparedStatement maj = con.prepareStatement("UPDATE  renseignement SET nom=?, date=? WHERE nom=?");
//                maj.setString(1, "nguidje");
//                maj.setString(2, "13-10-2022");
//                maj.setString(3, "flora");
//                maj.executeUpdate();
//                System.out.println("mise a jours");
//
//            } catch (SQLException e) {
//                System.out.println("maj echoue");
//            }
            miseJours("guilene","12-34-3333","herve");
            System.out.println("la connexion a reussie");

            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }


    }

    public static void insererRenseignement(String nom, String date) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO renseignement (nom, date) VALUES (?, ?)");
            statement.setString(1, nom);
            statement.setString(2, date);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void supprimerRenseignement(String nom) {

        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM renseignement WHERE nom = ?");
            statement.setString(1, nom);
            statement.executeUpdate();
            System.out.println("suppression reussie");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void recherche(String nom) {

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT*FROM renseignement WHERE nom=?");
            statement.setString(1, nom);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                System.out.println("id: " + result.getInt("id"));
                System.out.println("nom " + result.getString("nom"));
                System.out.println("date " + result.getString("date"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void miseJours(String nom, String date, String ancnom) {

        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE renseignement set nom=?, date=? WHERE nom=? ");
            statement.setString(1, nom);
            statement.setString(2, date);
            statement.setString(3, ancnom);
            System.out.println("mise a jours reussie");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void affiche() {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT *FROM renseignement");
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                System.out.println("id: " + result.getInt("id"));
                System.out.println("nom " + result.getString("nom"));
                System.out.println("date " + result.getString("date"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
