package etudiant.controllers;

import etudiant.Modele.RenseignementRepository;

import java.sql.*;
import static etudiant.vue.Vue.*;


public class Etudiant {


    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        try (  Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/etudiant", "root", "")){
          ;
            final RenseignementRepository renseignementRepository = new RenseignementRepository(con);
            //con.createStatement().executeUpdate("DELETE FROM renseignement");

            int choix = 0;

            while (0!=(choix = RenseignementRepository.choix())) {
                switch (choix) {
                    case 1:
                        insertion(renseignementRepository);
                        break;

                    case 2:
                        rechercher(renseignementRepository);
                        break;

                    case 3:
                        suppression(renseignementRepository);
                        break;

                    case 4:
                        miseJours(renseignementRepository);
                        break;

                    default:
                        System.out.println("choix inexistant");
                }
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}

