package etudiant.vue;

import etudiant.Modele.Renseignement;
import etudiant.Modele.RenseignementRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class Vue {
    public static void miseJours(RenseignementRepository renseignementRepository) throws SQLException {
        System.out.println("TITRE_MISE_A JOURS");
        final Scanner scanner = new Scanner(System.in);

        System.out.println("entrer l'ancien nom");
        String ancienNom = scanner.nextLine();

        System.out.println("entrer le nouveau nom");
        String newnom = scanner.nextLine();

        System.out.println("entrer la nouvelle date");
        String newdate = scanner.nextLine();

        final Renseignement udpdateRen = new Renseignement(-1, newnom, newdate);
        final List<Renseignement> renseignementList = renseignementRepository.modifierParNom(ancienNom, udpdateRen);

        if (0 < renseignementList.size()) {
            System.out.println(" resultat de la mise a jours: " + renseignementList.size());
            for (final Renseignement renseignements : renseignementList) {
                System.out.println("---" + renseignements);
            }

        } else {
            System.out.println("aucun renseignement");
        }
    }

    public static void rechercher(RenseignementRepository renseignementRepository) throws SQLException {
        System.out.println("TITRE_RECHERCHE_PAR_NOM");
        final Scanner scanner = new Scanner(System.in);

        System.out.println("entrer le nom de l'etudiant a rechercher");
        String nomrech = scanner.nextLine();


        final List<Renseignement> renseignementList = renseignementRepository.rechercherParNom(nomrech);

        if (0 < renseignementList.size()) {
            System.out.println(" resultat de la recherche: " + renseignementList.size());
            for (final Renseignement renseignements : renseignementList) {
                System.out.println("---" + renseignements);
            }

        } else {
            System.out.println("aucun renseignement");
        }
    }


    public static void insertion(RenseignementRepository renseignementRepository) throws SQLException {
        System.out.println("TITRE_CREATION");
        final Scanner scanner = new Scanner(System.in);

        System.out.println("entrer le nom de l'etudiant a inserer ");
        String nomInsert = scanner.nextLine();

        System.out.println("entrer la date de naissance de l'etudiant ");
        String dateInsert = scanner.nextLine();

        final Renseignement etudInsert = new Renseignement(1, nomInsert, dateInsert);
        final Renseignement r = renseignementRepository.inserer(etudInsert);

    }

    public static void suppression(RenseignementRepository renseignementRepository) throws SQLException {
        System.out.println("TITRE_SUPPRESSION");
        final Scanner scanner = new Scanner(System.in);

        System.out.println("entrer le nom de l'etudiant a supprimer");
        String nomSupp = scanner.nextLine();

        boolean result = renseignementRepository.supprimerParNom(nomSupp);
        System.out.println(result);
    }
}
