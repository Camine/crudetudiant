package etudiant;

import etudiant.Modele.Renseignement;
import etudiant.Modele.RenseignementRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestRenseignement {
    public static void main(String[]args) throws SQLException, ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");
        final Connection connection = DriverManager
                .getConnection( "jdbc:mysql://localhost:3306/etudiant", "root", "" );
        final RenseignementRepository renseignementRepository =
                new RenseignementRepository( connection );

// Insertion
        Renseignement renseignement =
                new Renseignement(0, "Esther", "10/10/1910");
       // renseignementRepository.inserer( renseignement, ancienNom);

// Recherche par nom
        renseignementRepository.rechercherParNom( "Esther" );

// Suppression per nom
        renseignementRepository.supprimerParNom( "Esther" );

    }
}
