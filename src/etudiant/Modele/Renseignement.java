package etudiant.Modele;

public class Renseignement {
    private int id;
    private String nom;
    private String date;

    public Renseignement(int id, String nom, String date) {
        this.id = id;
        this.nom = nom;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String toString(){
        return String.format("renseignement= { id=%d, nom=%s, date=%s }", id, nom, date);
    }
}
