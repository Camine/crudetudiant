package etudiant.Modele;

import etudiant.Modele.Renseignement;

import java.sql.*;
import java.util.*;

import static java.sql.Statement.RETURN_GENERATED_KEYS;


public class RenseignementRepository {
    private  final Connection connection;


    public RenseignementRepository(Connection connection) {
        this.connection = connection;
    }

    public Renseignement inserer(final Renseignement enseignement) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO renseignement (nom, date) VALUES (?, ?)",RETURN_GENERATED_KEYS)) {

            statement.setString(1, enseignement.getNom());
            statement.setString(2, enseignement.getDate());
            statement.executeUpdate();
            try (final ResultSet genarateKey = statement.getGeneratedKeys()) {
                if (genarateKey.next()) {
                    final int id = genarateKey.getInt(1);
                    Renseignement etudInsert=new Renseignement(id, enseignement.getNom(), enseignement.getDate());
                    return etudInsert;
                } else {
                    throw new RuntimeException("cannot retrieves last Id");
                }

            }
        }
    }

    public List<Renseignement> rechercherParNom(String nom) throws SQLException {

        try (final PreparedStatement statement = connection.prepareStatement("SELECT*FROM renseignement WHERE nom=?")) {
            statement.setString(1, nom);
            try (final ResultSet result = statement.executeQuery()) {
                final List<Renseignement> results = new ArrayList<>();

                while (result.next()) {
                    final int id = result.getInt("id");
                    nom = result.getString("nom");
                    final String date = result.getString("date");
                    final Renseignement renseignement = new Renseignement(id, nom, date);
                    results.add(renseignement);

                }
                return results;
            }
        }
    }

    public boolean supprimerParNom(String nom) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM renseignement WHERE nom = ?")) {
            statement.setString(1, nom);
            return 0 < statement.executeUpdate();
        }
    }

    public List<Renseignement> modifierParNom(String ancienNom, Renseignement enseignement) throws SQLException {
        List<Renseignement> r = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT*FROM renseignement WHERE nom=?")) {
            statement.setString(1, ancienNom);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()){
                if ((result.getString("nom")).equals(ancienNom)) {
                    try (PreparedStatement stat = connection.prepareStatement("UPDATE renseignement set nom=?, date=? WHERE nom=? ")) {
                        stat.setString(1, enseignement.getNom());
                        stat.setString(2, enseignement.getDate());
                        stat.setString(3, ancienNom);
                        stat.executeUpdate();
                    }
                }
                    r.add(enseignement);
                }
            }
        }
        return r;
    }

    public static int choix() {
        final Scanner scanner = new Scanner(System.in);

        System.out.println("OPTION");
        System.out.println("0. QUITTER");
        System.out.println("1: TITRE_CREATION");
        System.out.println("2: TITRE_RECHERCHE");
        System.out.println("3: TITRE_SUPPRESSION");
        System.out.println("4: TITRE_MISE_A_JOURS");
        System.out.println("veillez entrer votre choix");

        for (; ; ) {
            final int choix = scanner.nextInt();

            if (0 <= choix && choix <= 4) {
                if (0==choix) {
                    System.exit(0);
                }
                return choix;
            }
        }

    }
}
